# live-build configuration for Kali ISO images


- l’Université de Saïda Dr. Moulay Tahar,

- l’Université Ahmed Draia  Adrar,

- l’Université de Jijel – Mohammed Seddik Benyahia

- sous la direction de la

- Commission Nationale Logiciels Libres »,

Organisent:

## Le premier concours de création d’une distribution Linux Algérienne.
### Objectif du concours : Création de la première distribution Linux algérienne, alliant convivialité, sécurité, performance, stabilité et pouvant être personnalisée selon les secteurs

## Développement de la distribution :
Les personnes sélectionnées individuellement ou par groupe seront contactées par e-mail ou SMS et auront jusqu’au début du concours pour se préparer à participer au Hackathon qui se déroulera les 29 et 30 Octobre 2024 à l’Université de Saïda Dr Moulay Tahar.
## Cahier des charges :
Le concours sera régit par un cahier des charges qui décrira dans le détail ce qui est attendu des participants et du projet qu’ils présenteront.
## Sélection des participants :
Les participants au hackathon sélectionnés seront soumis à l’expertise d’un jury formé d’experts dans le domaine qui prononceront le résultat final (les trois (03) premiers gagnants du concours) le 31 Octobre 2024.

Prise en charge des participants : L’Université de Saïda Dr. Moulay Tahar prendra en charge entièrement les participants sélectionnés au hackathon (hébergement, restauration, pause-café, … etc) et la logistique nécessaire pour toute la durée du hackathon.

Récompenses : Les meilleurs projets seront primés comme suit :

– 1er Prix : 2 500 000,00 DA

– 2ème Prix : 1 500 000,00 DA

– 3ème Prix : 1 000 000,00 DA
Le concours est sponsorisé par de grandes entreprises économiques nationales, publiques et privées.

![img_1.png](img_1.png)

## mes notes
https://1drv.ms/w/c/685b455decd3debc/ESHuIgFnsrVFi02KVVsiczcBqe8rBYIZ2Oub0dNNSWGpkA

# live-build configuration for Kali ISO images

Have a look at [Live Build a Custom Kali ISO](https://www.kali.org/docs/development/live-build-a-custom-kali-iso/) for explanations on how to use this repository.

Get Ubuntu Server from https://ubuntu.com/download/server
Ubuntu 24.04 LTS

VMware Workstation Pro: Now Available Free for Personal Use https://blogs.vmware.com/workstation/2024/05/vmware-workstation-pro-now-available-free-for-personal-use.html

WinSCP 6.3 Download https://winscp.net/eng/download.php

~~~~ _~~ 1.	Activer WSL sur Windows 10 ou 11 :
      o	Ouvrez PowerShell en tant qu’administrateur.
      o	Exécutez la commande wsl --install.
      o	Redémarrez votre ordinateur.

1.	Mettre à jour vers WSL 2 :
      o	Après le redémarrage, ouvrez à nouveau PowerShell et exécutez wsl --set-default-version 2.
2.	Installer Kali Linux depuis le Microsoft Store :
      o	Recherchez “Kali Linux” dans le Microsoft Store.
      o	Installez Kali Linux.
3.	Configurer votre compte utilisateur Linux :
      o	Lorsque vous lancez Kali Linux, créez un compte utilisateur.
4.	Mettre à jour Kali Linux :
      o	Dans le terminal, exécutez
      sudo apt update && sudo apt upgrade.
      Maintenant, vous pouvez utiliser Kali Linux directement depuis Windows via WSL. N’hésitez pas à explorer les outils de sécurité et de test de pénétration disponibles

To run a command with root privileges, follow these steps:
1.	Open the Command Prompt or Windows PowerShell on your Windows 11 system

wsl --user charik


1.	Installer les outils nécessaires :
      o	Assurez-vous d’avoir installé les packages suivants sur votre système :~~_~~~~
~~~~

sudo apt update
ifconfig -> get the ip adress of ubunto server 
the open an invide de command
ssh charik@192.168.219.131

err ->
sudo dpkg --configure -a

[//]: # (sudo apt install git live-build simple-cdd cdebootstrap curl)

[//]: # (o	Vous pouvez également cloner les scripts de construction de Kali depuis le référentiel Git :)

[//]: # (git clone https://gitlab.com/kalilinux/build-scripts/live-build-config.git)

[//]: # (or)
git clone https://gitlab.com/Samie/live-build-config.git
Building a custom Kali Linux ISO involves several steps. Here’s a general guide to help you get started:

### Prerequisites

1. **Kali Linux Installed**: You should have Kali Linux installed on your system.
2. **Root Access**: You will need root or sudo access.
3. **Required Packages**: Install the necessary packages:
   ```bash
   sudo apt update
   sudo apt install live-build cdebootstrap
   ```

### Steps to Build a Kali ISO

1. **Create a Working Directory**:
   ```bash
   mkdir ~/kali-custom
   cd ~/kali-custom
   ```

2. **Set Up the Build Environment**:
   ```bash
   sudo ln -sf sid /usr/share/live/build/data/debian-cd/kali-rolling

   lb config
   ```

   You can add various options here, such as:
      - `--distribution kali-rolling`
      - `--debian-installer live`
      - `--mirror http://http.kali.org/kali`
      - `--variant live`

3. **Customize Your ISO**:
   You can modify the contents of the ISO by adding or removing packages. For example, to add a package:
   ```bash
   echo "package-name" >> config/package-lists/my.list.chroot
   ```

   To remove a package:
   ```bash
   echo "package-name" >> config/package-lists/my.list.chroot.remove
   ```

4. **Build the ISO**:
   Once you’ve configured everything, run:
   ```bash
   sudo lb build
   ```

   This process may take some time, depending on your system and chosen configurations.

5. **Locate Your ISO**:
   After the build completes, the ISO file will be located in the current directory, usually named something like `kali-linux-<date>-live.iso`.

### Testing Your ISO

You can test your ISO using a tool like VirtualBox or by creating a bootable USB drive using tools like `dd` or `Rufus`.

### Additional Customizations

- **Add Files**: You can add files to the ISO by placing them in the `config/includes.chroot` directory.
- **Change Boot Options**: Modify the `config/boot` files to adjust boot parameters.

### Conclusion

Building a custom Kali Linux ISO can be a powerful way to tailor your environment to your needs. For more advanced configurations, refer to the official Kali Linux documentation.

### Resources

- [Kali Linux Live Build Documentation](https://live-build.debian.net/)
- [Kali Linux Official Documentation](https://www.kali.org/docs/)
- 
cd live-build-config
git pull origin master


Great! If you’ve cloned a `live-build-config` directory, you can customize your Kali Linux ISO from there. Here’s how to proceed with it:

### Steps to Build Your Custom Kali ISO

1. **Navigate to the Config Directory**:
   ```bash
   cd /home/charik/live-build-config
   ```

2. **Update Configuration**:
   You can edit the configuration files to customize the build. Common files to check include:
      - `config/package-lists/`: Contains lists of packages to be included.
      - `config/includes.chroot/`: Place files you want to include in the ISO.
      - `config/boot/`: Modify boot parameters if needed.

3. **Build the ISO**:
   Run the build command from within the `live-build-config` directory:
   ```bash
   sudo lb build
   ```

4. **Monitor the Build Process**:
   The build process will output logs to the terminal. Watch for any errors or warnings.

5. **Locate Your ISO**:
   After the build completes successfully, the ISO file will be in the current directory, typically named something like `kali-linux-<date>-live.iso`.

### Testing Your ISO

- **Virtual Machine**: Use VirtualBox or VMware to test the ISO.
- **USB Drive**: Create a bootable USB using `dd` or a tool like `Rufus`.

### Example Customization

To add a package, you can edit a file in the `config/package-lists/` directory. For example:
```bash
echo "vim" >> config/package-lists/my.list.chroot
```

### Conclusion

With the cloned `live-build-config`, you can easily customize and build your own Kali ISO. Make sure to refer to the official documentation for more advanced features and configurations.

If you have specific customizations or issues you’d like help with, feel free to ask!

1.	Créer une image Live (pour essayer Kali sans altérer le système) :
      o	Accédez au répertoire live-build-config/ :
```bash
      cd live-build-config/
#   Exécutez le script de construction pour générer l’image Live (ISO) :
      ./build.sh –verbose
```


ERROR: You need live-build (>= 1:20230502+kali3), you have 3.0~a57-1ubuntu41.22.04.1
ERROR: You need debian-cd (>= 3.2.1+kali1), you have 3.1.35
wget https://http.kali.org/pool/main/k/kali-archive-keyring/kali-archive-keyring_2022.1_all.deb  404 not found
err fixed from this link
wget https://archive.kali.org/kali/pool/main/k/kali-archive-keyring/kali-archive-keyring-udeb_2024.1_all.udeb
wget https://http.kali.org/pool/main/l/live-build/live-build_20230502+kali3_all.deb 404 not found

dpkg-query: no packages found matching live-build

```bash
sudo apt install debootstrap
#ok
sudo dpkg -i kali-archive-keyring_2024.1_all.udeb
#err
sudo dpkg -i live-build_20230502+kali3_all.deb
#ok
sudo dpkg -i kali-archive-keyring-udeb_2024.1_all.udeb

cd ~/live-build-config

./build.sh –verbose

#Retrieving Packages …
#Ok
```
After building the image copy the iso from invite de commant ou utiliser WinScp
```
scp charik@192.168.219.131:/live-build-config/images/kali-linux-rolling-live-amd64.iso
```

1.	Attendez que le processus se termine. L’image Kali sera générée dans ./images/kali-linux-rolling-live-amd64.iso.
2.	Créer une image d’installation personnalisée :
      o	Si vous préférez une image d’installation, ajoutez l’option --installer :
      ./build.sh --verbose --installer

3.	Personnaliser votre ISO :
      o	Vous pouvez modifier divers aspects de votre ISO, comme ajouter des packages en dehors des dépôts Kali, configurer l’installation non assistée et

4.	changer le fond d’écran par défaut
ok
o	Explorez les options de configuration dans le répertoire live-build-config/.
5.	Tester votre ISO :
      o	Utilisez un logiciel de virtualisation (comme VirtualBox) pour tester votre ISO personnalisée.
      o	Installez-la sur une machine virtuelle et vérifiez que tout fonctionne correctement.
      N’oubliez pas que le développement d’un système d’exploitation est un processus complexe, mais avec de la patience et de la persévérance, vous pouvez créer votre propre version personnalisée de Kali Linu

